package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

class IocCasesTest {
    @Test
    void should_create_one_instance_using_different_bean_type() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        InterfaceOne one = context.getBean(InterfaceOne.class);
        InterfaceImpl two = context.getBean(InterfaceImpl.class);
        assertSame(one, two);
    }

    @Test
    void should_create_one_instance_for_class() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        BasicClass aClass = context.getBean(BasicClass.class);
        SubBasicClass sClass = context.getBean(SubBasicClass.class);
        assertSame(aClass, sClass);
    }

    @Test
    void should_create_one_instance_for_abstract_class() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstractBaseClass, derivedClass);
    }

    @Test
    void should_create_two_instance_for_prototype() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SimplePrototypeScopeClass scopeClass = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass anotherScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(scopeClass, anotherScopeClass);
    }

    @Test
    void should_create_after_getBean_for_prototype() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SimplePrototypeScopeClass scopeClass = context.getBean(SimplePrototypeScopeClass.class);
    }

    @Test
    void should_create_before_getBean_for_singleton() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_create_after_getBean_for_singleton_using_lazy() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        InterfaceImpl bean = context.getBean(InterfaceImpl.class);
    }

    @Test
    void should_create_two_instance_with_one_dependent() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        PrototypeScopeDependsOnSingleton prototypeScope = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton anotherPrototypeScope = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertNotSame(prototypeScope, anotherPrototypeScope);
        assertSame(prototypeScope.getSingletonDependent(), anotherPrototypeScope.getSingletonDependent());
    }

    @Test
    void should_create_one_instance_with_one_dependent() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonDependsOnPrototype singleton = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype anotherSingleton = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(singleton, anotherSingleton);
        assertSame(singleton.getPrototypeDependent(), anotherSingleton.getPrototypeDependent());
    }

    @Test
    void should_create_two_prototype_using_proxy() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonDependsOnPrototypeProxy proxy = context.getBean(SingletonDependsOnPrototypeProxy.class);
        String proxy1 = proxy.setProxy();
        SingletonDependsOnPrototypeProxy anotherProxy = context.getBean(SingletonDependsOnPrototypeProxy.class);
        String proxy2 = anotherProxy.setProxy();
        assertNotEquals(proxy1, proxy2);
    }

    @Test
    void should_create_two_with_batch_call() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonDependsOnPrototypeProxyBatchCall call = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        SingletonDependsOnPrototypeProxyBatchCall anotherCall = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        String callString = call.getToString();
        String anotherCallString = anotherCall.getToString();
        assertNotEquals(callString, anotherCallString);
    }

    @Test
    void should_create_proxy() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonDependsOnPrototypeProxy proxy = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy anotherProxy = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertSame(proxy, anotherProxy);
    }
}