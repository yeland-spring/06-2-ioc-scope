package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.annotation.Target;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class PrototypeDependent {
}
