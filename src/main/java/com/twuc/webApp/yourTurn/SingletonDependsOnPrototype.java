package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototype {
    private PrototypeDependent prototypeDependent;

    public SingletonDependsOnPrototype(PrototypeDependent prototypeDependent) {
        this.prototypeDependent = prototypeDependent;
    }

    public PrototypeDependent getPrototypeDependent() {
        return prototypeDependent;
    }
}
