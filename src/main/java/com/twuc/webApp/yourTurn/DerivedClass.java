package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class DerivedClass extends AbstractBaseClass {
    public DerivedClass() {
        System.out.println("singleton");
    }
}
