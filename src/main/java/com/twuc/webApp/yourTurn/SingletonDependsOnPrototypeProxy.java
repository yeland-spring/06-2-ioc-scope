package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return this.prototypeDependentWithProxy;
    }

    public String setProxy() {
        System.out.println(prototypeDependentWithProxy);
        return prototypeDependentWithProxy.toString();
    }
}
